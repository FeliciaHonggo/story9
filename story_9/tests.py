from django.test import TestCase, Client
from django.urls import resolve
from .views import story9, login
import unittest

# Create your tests here.
class Story9UnitTest(TestCase):
	def test_url(self):
		c = Client()
		response = c.get('')
		self.assertEqual(response.status_code, 200)

	# def test_template(self):
	# 	c = Client()
	# 	response = c.get('')
	# 	self.assertTemplateUsed(response, 'story9.html')

	# def test_story9_login(self):
	#  	c = Client()
 #        response = c.get('/login/')
 #        self.assertEqual(response.status_code, 200)

    # def test_story9_logout(self):
    # 	response = c.get('/logout/')
    #     self.assertEqual(response.status_code, 302)
