from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required


# Create your views here.

def story9(request):
	# form = AuthenticationForm(data=request.POST)
	return render(request, 'login.html')

# dari netninja wkwk
def login_view(request):
	error = "Terdapat kesalahan dalam memasukkan username ataupun password"
	# user = request.user
	# if user.is_authenticated:
	# 	return redirect('story_9:story9')
	if request.method == 'POST':
		form = AuthenticationForm(data=request.POST)
		if form.is_valid():
			#login the user
			user = form.get_user()
			login(request, user)
			return redirect('story_9:story9')
	else:
		form = AuthenticationForm()
	return render(request, 'story9.html', {'form':form, 'error':error})		


def logout_view(request):
		logout(request)
		return redirect('story_9:login')
