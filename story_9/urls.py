from django.urls import path
from . import views

app_name = 'story_9'

urlpatterns = [
	path('', views.story9, name= 'story9'),
	path('login/', views.login_view, name = 'login'),
	path('logout/', views.logout_view, name = 'logout'),

    
    # dilanjutkan ...
]

